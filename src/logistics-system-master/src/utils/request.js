import Vue from 'vue'
import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
//import { getToken } from '@/utils/auth'
import qs from 'qs'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 720000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    if (store.getters.token) {
      const dateTime = Date.now();
      const timestamp = Math.floor(dateTime / 1000);
      if (timestamp < store.getters.expireTime) {
        //console.log('token 可用');
        // let each request carry token
        // ['X-Token'] is a custom headers key
        // please modify it according to the actual situation
        //config.headers['Authorization'] = 'bearer ' + getToken()

        config.headers['Authorization'] = 'Bearer ' + store.getters.token
        config.paramsSerializer = params => {
          // Qs is already included in the Axios package
          return qs.stringify(params, {
            arrayFormat: 'brackets',
            encode: true
          })
        }
      }
      // else {
      //   //console.log('token 過期');
      //   // to re-login
      //   MessageBox.confirm(
      //     "您已被登出，取消以繼續停留在此頁或重新登入",
      //     "驗證過期",
      //     {
      //       confirmButtonText: "重新登入",
      //       cancelButtonText: "取消",
      //       type: "warning"
      //     }
      //   ).then(() => {
      //     store.dispatch("user/resetToken").then(() => {
      //       location.reload();
      //     });
      //   });
      // }
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data;
    // if the custom code is not 20000, it is judged as an error.
    if (response.status !== 200) {
      Message({
        message: response.message || "Error",
        type: "error",
        duration: 5 * 1000
      });
      return Promise.reject(new Error(res.message || "Error"));
    } else if (res.code === 400) {
      Message({
        message: res.msg || "Error",
        type: "error",
        duration: 5 * 1000
      });
    } else {
      return res;
    }
  },
  error => {
    if (error.response.status === 401) {
      // to re-login
      Vue.swal.fire({
        title: '全日物流股份有限公司',
        html:
          '<div style="font-size:30px;"><b>B2B運輸管理系痛</b></div>' +
          '<div style="font-size:24px;margin-top: 10px;">您登入的帳號沒有存取此頁面的權限，</br>請確認您的帳號權限。</div>' +
          '<button type="button" class="reLogin-confirm swal2-confirm swal2-styled" style="font-size:20px; margin:15px 0;">重新登入</button>' +
          '<div style="font-size:14px;">© 2021 全日物流股份有限公司 www.roundday.com.tw</div>' +
          '<div style="font-size:14px; margin-top: 10px;"><i class="el-icon-phone-outline"> (03)473-8800</i></div>' +
          '<div style="font-size:14px; margin-top: 10px;"><i class="ic-icon-mail"> service@roundday.com.tw</i></div>',
        showCancelButton: false,
        showConfirmButton: false,
        onBeforeOpen: () => {
          document.querySelector('.reLogin-confirm').addEventListener('click', () => {
            store.dispatch("user/resetToken").then(() => {
              location.reload();
            });
          })
        }
      })


      // MessageBox.confirm(
      //   "您已被登出，取消以繼續停留在此頁或重新登入",
      //   "驗證過期",
      //   {
      //     confirmButtonText: "重新登入",
      //     cancelButtonText: "取消",
      //     type: "warning"
      //   }
      // ).then(() => {
      //   store.dispatch("user/resetToken").then(() => {
      //     location.reload();
      //   });
      // });
    } else {
      Message({
        message: error.response.data.message || error.message,
        type: "error",
        duration: 5 * 1000
      });
    }
    return Promise.reject(error);
  }
);

export default service
