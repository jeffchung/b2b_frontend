import Cookies from 'js-cookie'

const TokenKey = 'vue_admin_template_token'

const dcCodeKey = 'dcCode'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getDcCode() {
  return Cookies.get(dcCodeKey)
}

export function setDcCode(dcCode) {
  return Cookies.set(dcCodeKey, dcCode)
}

export function removeDcCode() {
  return Cookies.remove(dcCodeKey)
}