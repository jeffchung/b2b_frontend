import request from '@/utils/request'

// 客服 --------------------------------------------------------------------------
// 客服 -> 匯出集貨總表 -> dc列表查詢 
export const dcListSearch = () => request.get('/b2b/api/dcs/collection_list');

// 客服 --------------------------------------------------------------------------
// 客服 -> 匯出集貨總表 -> 匯出報表
export const reportExport = (url) => request.get(url, { responseType: "blob" });