import request from '@/utils/request'

// 客服 --------------------------------------------------------------------------
// 客服 -> 訂單管理 -> 訂單查詢 「訂單編號」按鈕 // 61101201121B22
export const customerServiceOrderNumberSearch = orderNumber => request.get(`/b2b/api/customer_service/orders/${orderNumber}`);

// 客服 -> 訂單管理 -> 訂單查詢 「送貨單編號」按鈕 // S1SD2000043807
export const customerServiceDeliveryNoteNumberSearch = deliveryNoteNumber => request.get(`/b2b/api/customer_service/delivery_notes/${deliveryNoteNumber}`);

// 客服 -> 訂單管理 -> 「訂單查詢」按鈕
export const customerServiceOrdersSearch = (data) => request.post('/b2b/api/customer_service/orders', data);

// 客服 -> 訂單管理 -> 「查詢結果」表格中的「貨況」按鈕
export const getCargoCondition = id => request.get(`/b2b/api/customer_service/orders/${id}/order_flow`);

// 客服 -> 訂單管理 -> 下拉選單「配送物流士」資料
export const getDistributionLogistics = () => request.get('/b2b/api/hrs_staff/drivers/short');

// 客戶 --------------------------------------------------------------------------
// 客戶 -> 訂單管理 -> 訂單查詢 「訂單編號」按鈕 // 1080624-025
export const customerOrderNumberSearch = (goodsOwner, orderNumber) => request.get(`/b2b/api/customer/${goodsOwner}/orders/${orderNumber}`);

// 客戶 -> 訂單管理 -> 訂單查詢 「送貨單編號」按鈕 // N2GM2000304292
export const customerDeliveryNoteNumberSearch = (goodsOwner, deliveryNoteNumber) => request.get(`/b2b/api/customer/${goodsOwner}/delivery_notes/${deliveryNoteNumber}`);

// 客戶 -> 訂單管理 -> 「訂單查詢」按鈕
export const customerOrdersSearch = (username, data) => request.post(`/b2b/api/customer/${username}/orders`, data);

// 客戶 -> 訂單管理 -> 「查詢結果」表格中的「貨況」按鈕
export const getCustomerCargoCondition = id => request.get(`/b2b/api/customer/orders/${id}/order_flow`);

// 客戶 -> 訂單管理 -> 委運客戶列表
export const getCustomerCommission = username => request.get(`/b2b/api/customers/${username}/codes`);

// 未回報訂單查詢 --------------------------------------------------------------------------
// 訂單管理 -> 未回報訂單查詢 ->「查詢」按鈕
export const unreportedOrderSearch = (data) => request.post('/b2b/api/customer_service/orders/unreported', data);

// 其他 --------------------------------------------------------------------------
// 訂單管理 -> 委運客戶列表（元件）
export const getCustomersByCode = code => request.get(`/b2b/api/customers/codes/${code}`);

// 訂單管理 ->「貨況」表格中的「車號/櫃號」欄位 -> 新增下載車溫檔案
export const getCarTemperatureFile = (id) => request.get(`/b2b/api/cars/temperature?operationLogId=${id}`, {
    responseType: "blob"
});

// 訂單管理 ->「貨況」表格中的「異常記錄」欄位 - 取得異常紀錄照片
export const getAbnormalRecord = id => request.get(`/b2b/api/abnormal_record/${id}`);

// 訂單管理 ->「貨況」表格中的「異常記錄」欄位 - 下載異常紀錄照片
export const getAbnormalRecordPhotos = id => request.get(`/b2b/api/abnormal_record/photos/${id}`, {
    responseType: 'blob'
});