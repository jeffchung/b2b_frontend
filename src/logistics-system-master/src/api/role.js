import request from '@/utils/request'


export function getRoutes() {
  return request({
    url: '/vue-admin-template/routes',
    method: 'get'
  })
}

// 角色管理 -> 取得所有角色資訊
export const getRoles = () => request.get('/cas/api/roles');

// 角色管理 -> 刪除角色
export function deleteRole(roleId) {
  return request.delete("cas/api/roles/" + roleId);
}

// 角色管理 -> 建立角色
export function createRole(role) {
  return request.post("cas/api/roles/", role);
}

// 角色管理 -> 編輯角色
export const updateRole = (roleId, data) => request.put(`cas/api/roles/${roleId}`, data);