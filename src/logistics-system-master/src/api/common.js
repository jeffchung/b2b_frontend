import request from '@/utils/request'

// 物流士與車輛管理 -> 「公司別」下拉選單
export const getCompany = (dcNo) => request.get(`b2b/api/company?dcNo=${dcNo}`);

// 物流中心」列表
export const getDcs = () => request.get('b2b/api/dcs');

// 異常管理 -> 「委運客戶」列表
export const getCustomers = () => request.get('b2b/api/customers/codes');
