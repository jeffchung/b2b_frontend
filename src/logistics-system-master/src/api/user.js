import request from '@/utils/request'

// 登入
export function login(data) {
  return request.post("/cas/api/auth/login", data);
}

// web 登入驗證
export function webLogin(data) {
  return request.post("b2b/api/auth/web/login", data);
}

// 用戶管理 -> 取得所有用戶資訊
export const getUsers = () => request.get('/cas/api/users');

// 用戶管理 -> 取得角色列表(下拉選單使用)
export const getRoles = () => request.get('/cas/api/roles/short');

// 用戶管理 -> 刪除用戶
export function deleteUser(userId) {
  return request.delete("cas/api/users/" + userId);
}

// 用戶管理 -> 建立用戶
export function createUser(user) {
  return request.post("cas/api/users/", user);
}

// 用戶管理 -> 編輯用戶
export const updateUser = (userId, data) => request.put(`cas/api/users/${userId}`, data);

// 取得角色權限
export const getUserPermission = (roleIds) => request.get(`/cas/api/roles/resources?serverId=1&roleIds=${roleIds}`);

// 使用者帳號設定功能
export const userAccountSetting = (data) => request.patch('/b2b/api/hrs_staff/password', data);