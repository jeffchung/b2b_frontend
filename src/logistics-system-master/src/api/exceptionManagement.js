import request from '@/utils/request'

// 區間異常聯絡表 --------------------------------------------------------------
// 異常管理 -> 區間異常聯絡表 「訂單編號」按鈕 // 231-20210506019
export const intervalAbnormalContactFormOrderNumberSearch = orderNumber => request.get(`/b2b/api/interval_abnormal_record/orders/${orderNumber}`);

// 異常管理 -> 區間異常聯絡表 「送貨單編號」按鈕 // S1GD2100017564
export const intervalAbnormalContactFormDeliveryNoteNumberSearch = deliveryNoteNumber => request.get(`/b2b/api/interval_abnormal_record/delivery_notes/${deliveryNoteNumber}`);

// 異常管理 -> 區間異常聯絡表 「查詢」按鈕
export const intervalAbnormalContactFormSearch = (data) => request.post('/b2b/api/interval_abnormal_record', data);

// 異常管理 -> 區間異常聯絡表 -> 查詢結果中的「處理記錄」按鈕
export const getProcessingRecords = (id) => request.get(`/b2b/api/interval_abnormal_record/${id}`);

// 異常管理 -> 區間異常聯絡表 -> 查詢結果中的「處理結果」視窗中的「新增紀錄」
export const createProcessingRecordsById = (id, data) => request.post(`/b2b/api/interval_abnormal_record/${id}/log`, data);

// 異常管理 -> 區間異常聯絡表 -> 查詢結果中的「處理結果」視窗中的「結案」
export const closeProcessingRecordsById = (id, data) => request.patch(`/b2b/api/interval_abnormal_record/${id}/close`, data);

// 異常管理 -> 區間異常聯絡表 -> 查詢結果中的「處理結果」視窗中的「更新 受文DC & 車號/板架號」
export const editProcessingRecordsById = (id, data) => request.patch(`/b2b/api/interval_abnormal_record/${id}`, data);

// 異常管理 -> 區間異常聯絡表 -> 「匯出區間彙總報表」功能
export const exportReportProcessingRecords = (dcNo, startDate, endDate) => request.get(`/b2b/api/interval_abnormal_record/summary_statement/?dcNo=${dcNo}&startDate=${startDate}&endDate=${endDate}`, {
    responseType: "blob"
});

// 異常管理 -> 區間異常聯絡表 -> 「匯出區間異常聯絡表」功能
export const exportPdfProcessingRecords = (ids) => request.get(`/b2b/api/interval_abnormal_record/pdf?id=${ids}`, {
    responseType: "blob"
});

// 貨品包裝確認單查詢 --------------------------------------------------------------
// 異常管理 -> 「貨品包裝確認單查詢」按鈕
export const goodsPackageSearch = (custName, startDate, endDate) => request.get(`/b2b/api/packing_list?custName=${custName}&startDate=${startDate}&endDate=${endDate}`);

// 異常管理 -> 貨品包裝確認單查詢 -> 查詢結果中的「檢視」按鈕
export const getGoodsPackageListById = (id) => request.get(`/b2b/api/packing_list/${id}`);

// 異常管理 -> 貨品包裝確認單查詢 -> 查詢結果中的「檢視」頁面 -> 下載貨品包裝照片
export const getPackingListPhotos = id => request.get(`/b2b/api/packing_list/photo/${id}`, {
    responseType: 'blob'
});

// 異常管理 -> 貨品包裝確認單查詢 -> 查詢結果中的「檢視」頁面 -> 下載 PDF
export const getPackingListPdf = id => request.get(`/b2b/api/packing_list/${id}/pdf`, {
    responseType: 'blob'
});