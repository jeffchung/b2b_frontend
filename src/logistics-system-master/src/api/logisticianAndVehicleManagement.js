import request from '@/utils/request'

// 物流士管理 --------------------------------------------------------------
// 物流士與車輛管理 -> 物流士管理 -> 查詢物流士
export const searchLogistician = (dcNo, userNo, userName, company) => request.get(`b2b/api/hrs_staff/drivers?dcNo=${dcNo}&userNo=${userNo}&userName=${userName}&company=${company}`);

// 物流士與車輛管理 -> 物流士管理 -> 新增物流士
export const createLogistician = (data) => request.post('b2b/api/hrs_staff/drivers', data);

// 物流士與車輛管理 -> 物流士管理 -> 刪除物流士
export function deleteLogistician(logisticianId) {
    return request.delete("b2b/api/hrs_staff/" + logisticianId);
}

// 物流士與車輛管理 -> 物流士管理 -> 修改物流士名稱
export const editLogistician = (logisticianId, data) => request.patch(`b2b/api/hrs_staff/${logisticianId}`, data);

// 車輛管理 --------------------------------------------------------------
// 物流士與車輛管理 -> 車輛管理 -> 查詢車輛
export const searchVehicle = (dcNo, carType, carNo, wt, company) => request.get(`b2b/api/cars?dcNo=${dcNo}&carType=${carType}&carNo=${carNo}&wt=${wt}&company=${company}`);

// 物流士與車輛管理 -> 車輛管理 -> 新增車輛
export const createVehicle = (data) => request.post('b2b/api/cars', data);

// 物流士與車輛管理 -> 車輛管理 -> 刪除車輛
export function deleteVehicle(carId) {
    return request.delete("b2b/api/cars/" + carId);
}