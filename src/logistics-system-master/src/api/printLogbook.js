import request from "@/utils/request";

// 物流士列印行車日誌 -> 「查詢」按鈕
export const driversDailyLogSearch = (area, queryNumber) => request.get(`/b2b/api/dcs/${area}/users/${queryNumber}/drivers_daily_log`);

// 物流士列印行車日誌 -> 取得「行車日誌」(表格「查詢」按鈕)
export const getDriversDailyLogPdf = (area, queryNumber, carNo, date) => request.get(`/b2b/api/dcs/${area}/users/${queryNumber}/drivers_daily_log/pdf?carNo=${carNo}&date=${date}`, {
    responseType: "blob"
});