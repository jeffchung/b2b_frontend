import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true // (默認 false)    當設置 true 的時候該路由不會在側邊欄出現 如401，login 等頁面，或者如一些編輯頁面 /edit/1
 *
 * alwaysShow: true               當你一個路由下面的 children 聲明的路由大於1個時，自動會變成嵌套的模式--如組件頁面
 *                                只有一個時，會將那個子路由當做根路由顯示在側邊欄--如引導頁面
 *                                若你想不管路由下面的 children 聲明的個數都顯示你的根路由
 *                                你可以設置 alwaysShow: true，這樣它就會忽略之前定義的規則，一直顯示根路由
 *
 * redirect: noRedirect           當設置 noRedirect 的時候該路由在面包屑導航中不可被點擊
 *
 * name:'router-name'             設定路由的名字，一定要填寫不然使用 <keep-alive> 時會出現各種問題
 * meta : {
    roles: ['admin','editor']    設置該路由進入的權限，支持多個權限疊加
    title: 'title'               設置該路由在側邊欄和麵包屑中展示的名字
    icon: 'svg-name'/'el-icon-x' 設置該路由的圖標，支持 svg-class，也支持 el-icon-x element-ui 的 icon
    noCache: true                如果設置為 true，則不會被 <keep-alive> 緩存(默認 false )
    breadcrumb: false            如果設置為 false，則不會 在breadcrumb 麵包屑中顯示(默認 true )
    affix: true // 若果設置為true，它則會固定在 tags-view 中(默認 false )
    activeMenu: '/example/list'  當路由設置了該屬性，則會高亮相對應的側邊欄。
 *                               這在某些場景非常有用，比如：一個文章的列表頁路由為：/article/list
 *                               點擊文章進入文章詳情頁，這時候路由為 /article/1，但你想在側邊欄高亮文章列表的路由，就可以進行如下設置
  }

 * // 範例
 *{
    key: '****', //(自定義)全日物流系統權限使用
    titleForRoleTree: 'permission', //(自定義)系統管理 -> 角色管理 中的新增/編輯 Dialog 中的 el-tree 所用的多語系 -> roleTree
    path: '/permission',
    component: Layout,
    redirect: '/permission/index', // 重定向地址，在面包屑中點擊會重定向去的地址
    hidden: true, // 不在側邊欄線上
    alwaysShow: true, // 一直顯示根路由
    meta: { roles: ['admin','editor'] }, // 你可以在根路由設置權限，這樣它下面所以的子路由都繼承了這個權限
    children: [{
      path: 'index',
      component: ()=>import('permission/index'),
      name: 'permission',
      meta: {
        title: 'permission', // 側邊欄所用的多語系 -> route
        icon: 'svg-name'/'el-icon-x' 設置該路由的圖標，支持 svg-class，也支持 el-icon-x element-ui 的 icon
        roles: ['admin','editor'], // 或者你可以給每一個子路由設置自己的權限
        noCache: true // 不會被 <keep-alive> 緩存
      }
    }]
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 * 代表那些不需要動態判斷權限的路由，如登錄頁、404、等通用頁面。
 */


export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/printLogbook',
    component: () => import('@/views/printLogbook/index'),
    hidden: true
  },

  {
    path: '/showDriverPassBarcode',
    component: () => import('@/views/logisticianAndVehicleManagement/logistician/showDriverPassBarcode'),
    hidden: true
  },

  {
    key: '1000', // 首頁
    titleForRoleTree: 'dashboard',
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: {
        title: 'dashboard',
        icon: 'dashboard',
        affix: true,
      }
    }]
  },
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 * 代表那些需求動態判斷權限並通過 addRoutes 動態添加的頁面。
 */
export const asyncRoutes = [
  {
    key: '2000', // 系統管理
    childrenKey: ['2100', '2200', '2300'],
    titleForRoleTree: 'systemManagement',
    path: '/systemManagement',
    component: Layout,
    redirect: '/systemManagement',
    name: 'SystemManagement',
    hidden: false, // 不在側邊欄線上
    alwaysShow: true, // 一直顯示根路由
    meta: {
      title: 'systemManagement',
      icon: 'ic-icon-home',
      roles: []
    },
    children: [
      {
        key: '2100', // 用戶管理
        titleForRoleTree: 'user',
        path: 'user',
        component: () => import('@/views/systemManagement/user/index'),
        name: 'User',
        meta: {
          title: 'user',
          roles: []
        },
      },
      {
        key: '2200', // 角色管理
        titleForRoleTree: 'role',
        path: 'role',
        component: () => import('@/views/systemManagement/role/index'),
        name: 'Role',
        meta: {
          title: 'role',
          roles: []
        },
      },
      {
        key: '2300', // 使用者帳號設定
        titleForRoleTree: 'userAccount',
        path: 'userAccount',
        component: () => import('@/views/systemManagement/userAccount/index'),
        name: 'userAccount',
        meta: {
          title: 'userAccount',
          roles: []
        },
      },
    ]
  },

  {
    key: '3000', // 訂單管理
    childrenKey: ['3100', '3200', '3300'],
    titleForRoleTree: 'orderManagement',
    path: '/orderManagement',
    component: Layout,
    redirect: '/orderManagement',
    name: 'OrderManagement',
    alwaysShow: true, // 一直顯示根路由
    meta: {
      title: 'orderManagement',
      icon: 'ic-icon-file-text1',
      roles: []
    },
    children: [
      {
        key: '3100', // [客服]訂單管理
        titleForRoleTree: 'orderTrackingForCustomerService',
        path: 'customerServiceOrderTracking',
        component: () => import('@/views/orderManagement/orderTracking/customerService/index'),
        name: 'CustomerServicerOderTracking',
        meta: {
          title: 'customerServiceOrderTracking',
          roles: []
        },
      },
      {
        key: '3200', // [客戶]訂單查詢
        titleForRoleTree: 'orderTrackingForCustomer',
        path: 'customerOrderTracking',
        component: () => import('@/views/orderManagement/orderTracking/customer/index'),
        name: 'CustomerOrderTracking',
        meta: {
          title: 'customerOrderTracking',
          roles: []
        },
      },
      {
        key: '3300', // 未回報訂單查詢
        titleForRoleTree: 'unreportedOrders',
        path: 'unreportedOrders',
        component: () => import('@/views/orderManagement/unreportedOrders/index'),
        name: 'UnreportedOrders',
        meta: {
          title: 'unreportedOrders',
          roles: []
        },
      },
    ]
  },

  {
    key: '4000', // 物流士與車輛管理
    childrenKey: ['4100', '4200'],
    titleForRoleTree: 'logisticianAndVehicleManagement',
    path: '/logisticianAndVehicleManagement',
    component: Layout,
    redirect: '/logisticianAndVehicleManagement',
    name: 'LogisticianAndVehicleManagement',
    alwaysShow: true, // 一直顯示根路由
    meta: {
      title: 'logisticianAndVehicleManagement',
      icon: 'ic-icon-file-text1',
      roles: []
    },
    children: [
      {
        key: '4100', // 物流士管理
        titleForRoleTree: 'logisticianManagement',
        path: 'logisticianManagement',
        component: () => import('@/views/logisticianAndVehicleManagement/logistician/index'),
        name: 'LogisticianManagement',
        meta: {
          title: 'logisticianManagement',
          roles: []
        },
      },
      {
        key: '4200', // 車輛管理
        titleForRoleTree: 'vehicleManagement',
        path: 'vehicleManagement',
        component: () => import('@/views/logisticianAndVehicleManagement/vehicle/index'),
        name: 'VehicleManagement',
        meta: {
          title: 'vehicleManagement',
          roles: []
        },
      },
    ]
  },

  {
    key: '5000', // 異常管理
    childrenKey: ['5100', '5200'],
    titleForRoleTree: 'exceptionManagement',
    path: '/exceptionManagement',
    component: Layout,
    redirect: '/exceptionManagement',
    name: 'ExceptionManagement',
    alwaysShow: true, // 一直顯示根路由
    meta: {
      title: 'exceptionManagement',
      icon: 'ic-icon-file-text1',
      roles: []
    },
    children: [
      {
        key: '5100', // 貨品包裝確認單查詢
        titleForRoleTree: 'goodsPackageReceiptSearch',
        path: 'goodsPackageReceiptSearch',
        component: () => import('@/views/exceptionManagement/goodsPackageReceiptSearch/index'),
        name: 'GoodsPackageReceiptSearch',
        meta: {
          title: 'goodsPackageReceiptSearch',
          roles: []
        },
      },
      {
        key: '5200', // 區間異常聯絡表
        titleForRoleTree: 'intervalAbnormalContactForm',
        path: 'intervalAbnormalContactForm',
        component: () => import('@/views/exceptionManagement/intervalAbnormalContactForm/index'),
        name: 'IntervalAbnormalContactForm',
        meta: {
          title: 'intervalAbnormalContactForm',
          roles: []
        },
      },
    ]
  },

  {
    key: '6000', // 報表管理
    childrenKey: ['6100'],
    titleForRoleTree: 'reportManagement',
    path: '/reportManagement',
    component: Layout,
    redirect: '/reportManagement',
    name: 'reportManagement',
    alwaysShow: true, // 一直顯示根路由
    meta: {
      title: 'reportManagement',
      icon: 'ic-icon-file-text1',
      roles: []
    },
    children: [
      {
        key: '6100', // 集貨總表
        titleForRoleTree: 'collectionSummary',
        path: 'collectionSummary',
        component: () => import('@/views/reportManagement/collectionSummary/index'),
        name: 'collectionSummary',
        meta: {
          title: 'collectionSummary',
          roles: []
        },
      },
    ]
  },

  { path: '*', redirect: '/404', hidden: true }
]

export const errorRoutes = [
  // {
  //   id: '1000',
  //   path: '/permission',
  //   component: Layout,
  //   redirect: '/permission/role',
  //   name: 'Permission',
  //   meta: {
  //     title: 'permission',
  //     icon: 'lock',
  //     roles: ['admin', 'editor'] // 需由後端塞入
  //   },
  //   children: [
  //     {
  //       id: '1100',
  //       path: 'user',
  //       component: () => import('@/views/permission/user'),
  //       name: 'UserPermission',
  //       meta: {
  //         title: 'userPermission',
  //         roles: ['admin', 'editor'] // 需由後端塞入
  //       },
  //       button: [
  //         {
  //           id: '1110',
  //           name: 'add',
  //           roles: ['admin', 'editor'] // 需由後端塞入
  //         },
  //         {
  //           id: '1120',
  //           name: 'edit',
  //           roles: ['admin', 'editor'] // 需由後端塞入
  //         },
  //         {
  //           id: '1130',
  //           name: 'delete',
  //           roles: ['admin', 'editor'] // 需由後端塞入
  //         }
  //       ],
  //     },
  //   ]
  // },

  // {
  //   path: 'adminOnly',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
  //       meta: { title: 'Admin Only', roles: ['admin'], icon: 'link' }
  //     }
  //   ]
  // },

  // {
  //   path: 'editorOnly',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
  //       meta: { title: 'Editor Only', roles: ['editor'], icon: 'link' }
  //     }
  //   ]
  // },

  // {
  //   path: 'blueOnly',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
  //       meta: { title: 'Blue Only', roles: ['blue'], icon: 'link' }
  //     }
  //   ]
  // },

  // {
  //   path: 'kuOnly',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
  //       meta: { title: 'ku Only', roles: ['ku'], icon: 'link' }
  //     }
  //   ]
  // },

  // 404 page must be placed at the end !!!
  //{ path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
