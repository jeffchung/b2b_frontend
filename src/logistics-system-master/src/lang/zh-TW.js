export default {
    // 側邊欄所用的多語系
    route: {
        dashboard: '首頁',

        systemManagement: '系統管理',
        user: '用戶管理',
        role: '角色管理',
        userAccount: '使用者帳號設定',

        orderManagement: '訂單管理',
        customerServiceOrderTracking: '訂單查詢',
        customerOrderTracking: '訂單查詢',
        unreportedOrders: '未回報訂單查詢',

        logisticianAndVehicleManagement: '物流士與車輛管理',
        logisticianManagement: '物流士管理',
        vehicleManagement: '車輛管理',

        exceptionManagement: '異常管理',
        goodsPackageReceiptSearch: '貨品包裝確認單查詢',
        intervalAbnormalContactForm: '區間異常聯絡表',

        reportManagement: '報表管理',
        collectionSummary: '集貨總表',

        example: '範例',
        table: '表格',
        tree: '樹狀結構',
        nested: '路徑',
        menu1: '選單 1',
        'menu1-1': '選單 1-1',
        'menu1-2': '選單 1-2',
        'menu1-2-1': '選單 1-2-1',
        'menu1-2-2': '選單 1-2-2',
        'menu1-3': '選單 1-3',
        menu2: '選單 2',
        tab: 'Tab',
        form: '表單',
        page401: '401',
        page404: '404',
        externalLink: '快速連結',
        permission: '權限設定',
        rolePermission: '角色權限',
    },
    // 系統管理 -> 角色管理 中的新增/編輯 Dialog 中的 el-tree 所用的多語系
    roleTree: {
        dashboard: '首頁',

        systemManagement: '系統管理',
        user: '用戶管理',
        role: '角色管理',
        userAccount: '使用者帳號設定',

        orderManagement: '訂單管理',
        orderTrackingForCustomerService: '[客服]訂單管理',
        orderTrackingForCustomer: '[客戶]訂單查詢',
        unreportedOrders: '未回報訂單查詢',

        logisticianAndVehicleManagement: '物流士與車輛管理',
        logisticianManagement: '物流士管理',
        vehicleManagement: '車輛管理',

        exceptionManagement: '異常管理',
        goodsPackageReceiptSearch: '貨品包裝確認單查詢',
        intervalAbnormalContactForm: '區間異常聯絡表',

        reportManagement: '報表管理',
        collectionSummary: '集貨總表',
    },
    login: {
        title: '系統登入',
        logIn: '登入',
        username: '帳號',
        password: '密碼',
        printLogbook: '物流士列印行車日誌',
        any: '隨便填',
        thirdpartyTips: '本地不能模擬，請結合自己伺服器進行模擬！！！',
        switchLanguageSuccess: '切換繁體中文',
    },
    navbar: {
        dashboard: '首頁',
        logOut: '登出',
    },
    dashboard: {
        name: '當前登入者',
        roles: '角色'
    },
    tagsView: {
        close: '關閉',
        closeOthers: '關閉其他分頁',
        closeAll: '關閉全部分頁'
    },
    message: {
        success: {
            search: '查詢成功',
            accountUpdate: '用戶修改成功'
        },
        fail: {
            search: '查詢失敗',
        },
        waring: {
            deliveryNoteNumber: '警告！需填寫「送貨單號碼」',
            orderNumber: '警告！需填寫「訂單編號」',
            exportPdf: '請選取要匯出的區間異常聯絡表',
        },
        loading: {
            dataProcess: '資料處理中...'
        }
    },
    button: {
        export: '匯出',
        confirmUpdate: '確定更新',
    },
    pageContent: {
        consolidationList: '集貨總表',
        consolidationDC: '集貨DC',
        orderDate: '單據日期',
        deliveryDate: '配送日期',
        repassword: '密碼確認',
        oldPassword: '舊密碼',
        newPassword: '新密碼',
        loginAccount: '登入帳號',
        userName: '使用者名稱',
        to: '至',
        needInput: '請輸入',
        costcoDailyDistributionStatistics: '好市多每日配送統計表'
    }
}