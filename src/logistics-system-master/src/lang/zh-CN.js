export default {
    // 側邊欄所用的多語系
    route: {
        dashboard: '首页',

        systemManagement: '系统管理',
        user: '用户管理',
        role: '角色管理',
        userAccount: '使用者帐号设定',

        orderManagement: '订单管理',
        customerServiceOrderTracking: '订单查询',
        customerOrderTracking: '订单查询',
        unreportedOrders: '未回报订单查询',

        logisticianAndVehicleManagement: '物流士与车辆管理',
        logisticianManagement: '物流士管理',
        vehicleManagement: '车辆管理',

        exceptionManagement: '异常管理',
        goodsPackageReceiptSearch: '货品包装确认单查询',
        intervalAbnormalContactForm: '区间异常联络表',

        reportManagement: '报表管理',
        collectionSummary: '集货总表',

        example: '实例',
        table: '表格',
        tree: '树状结构',
        nested: '路由嵌套',
        menu1: '菜单 1',
        'menu1-1': '菜单 1-1',
        'menu1-2': '菜单 1-2',
        'menu1-2-1': '菜单 1-2-1',
        'menu1-2-2': '菜单 1-2-2',
        'menu1-3': '菜单 1-3',
        menu2: '菜单 2',
        tab: 'Tab',
        form: '表单',
        page401: '401',
        page404: '404',
        externalLink: '外链',
        permission: '权限设定',
        rolePermission: '角色权限',
    },
    // 系統管理 -> 角色管理 中的新增/編輯 Dialog 中的 el-tree 所用的多語系
    roleTree: {
        dashboard: '首页',

        systemManagement: '系统管理',
        user: '用户管理',
        role: '角色管理',
        userAccount: '使用者帐号设定',

        orderManagement: '订单管理',
        orderTrackingForCustomerService: '[客服]订单查询',
        orderTrackingForCustomer: '[客戶]订单查询',
        unreportedOrders: '未回报订单查询',

        logisticianAndVehicleManagement: '物流士与车辆管理',
        logisticianManagement: '物流士管理',
        vehicleManagement: '车辆管理',

        exceptionManagement: '异常管理',
        goodsPackageReceiptSearch: '货品包装确认单查询',
        intervalAbnormalContactForm: '区间异常联络表',

        reportManagement: '报表管理',
        collectionSummary: '集货总表',
    },
    login: {
        title: '系统登录',
        logIn: '登录',
        username: '账号',
        password: '密码',
        printLogbook: '物流士列印行车日志',
        any: '随便填',
        thirdpartyTips: '本地不能模拟，请结合自己业务进行模拟！！！',
        switchLanguageSuccess: '切换简体中文',
    },
    navbar: {
        dashboard: '首页',
        logOut: '退出登录',
    },
    dashboard: {
        name: '当前登录者',
        roles: '角色'
    },
    tagsView: {
        close: '关闭',
        closeOthers: '关闭其他分页',
        closeAll: '关闭全部分页'
    },
    message: {
        success: {
            search: '查询成功',
            accountUpdate: '用户修改成功'
        },
        fail: {
            search: '查询失败',
        },
        waring: {
            deliveryNoteNumber: '警告！需填写「送货单号码」',
            orderNumber: '警告！需填写「订单编号」',
            exportPdf: '请选取要汇出的区间异常联络表',
        },
        loading: {
            dataProcess: '资料处理中...'
        }
    },
    button: {
        export: '汇出',
        confirmUpdate: '确定更新',
    },
    pageContent: {
        consolidationList: '集货总表',
        consolidationDC: '集货DC',
        orderDate: '单据日期',
        deliveryDate: '配送日期',
        repassword: '密码确认',
        oldPassword: '旧密码',
        newPassword: '新密码',
        loginAccount: '登入帐号',
        userName: '使用者名称',
        to: '至',
        needInput: '请输入',
        costcoDailyDistributionStatistics: '好市多每日配送统计表'
    }
}