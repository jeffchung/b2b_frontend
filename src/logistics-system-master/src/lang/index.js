import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Cookies from 'js-cookie'
import elementEnLocale from 'element-ui/lib/locale/lang/en' // element-ui lang
import elementZhCnLocale from 'element-ui/lib/locale/lang/zh-CN'// element-ui lang
import elementZhTwLocale from 'element-ui/lib/locale/lang/zh-TW'// element-ui lang
import enLocale from './en'
import zhTwLocale from './zh-TW'
import zhCnLocale from './zh-CN'

/**
 * Html templata 用法
 * <div>{{ $t("dashboard.name") }}</div>
 * js 用法
 * message: this.$t("dashboard.name"),
 */
Vue.use(VueI18n)

const messages = {
    en: {
        ...enLocale,
        ...elementEnLocale
    },
    zh_tw: {
        ...zhTwLocale,
        ...elementZhTwLocale
    },
    zh_cn: {
        ...zhCnLocale,
        ...elementZhCnLocale
    },
}

export function getLanguage() {
    const chooseLanguage = Cookies.get('language')
    if (chooseLanguage) return chooseLanguage

    // if has not choose language
    const language = (navigator.language || navigator.browserLanguage).toLowerCase()
    const locales = Object.keys(messages)
    for (const locale of locales) {
        if (language.indexOf(locale) > -1) {
            return locale
        }
    }
    return 'zh_tw'
}

const i18n = new VueI18n({
    // set locale
    // options: en | zh-TW | zh-CN
    locale: getLanguage(),
    // set locale messages
    messages
})

export default i18n