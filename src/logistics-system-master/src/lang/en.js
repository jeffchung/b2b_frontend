export default {
    // 側邊欄所用的多語系
    route: {
        dashboard: 'Dashboard',

        systemManagement: 'System Management',
        user: 'User',
        role: 'Role',
        userAccount: 'User Account',

        orderManagement: 'Order Management',
        customerServiceOrderTracking: 'Order Tracking',
        customerOrderTracking: 'Order Tracking',
        unreportedOrders: 'Unreported Orders Tracking',

        logisticianAndVehicleManagement: 'Logistician And Vehicle Management',
        logisticianManagement: 'Logistician Management',
        vehicleManagement: 'Vehicle Management',

        exceptionManagement: 'Exception Management',
        goodsPackageReceiptSearch: 'Goods Package Receipt Search',
        intervalAbnormalContactForm: 'Interval Abnormal Contact Form',

        reportManagement: 'Report Management',
        collectionSummary: 'Collection Summary',

        example: 'Example',
        table: 'Table',
        tree: 'Tree',
        nested: 'Nested Routes',
        menu1: 'Menu 1',
        'menu1-1': 'Menu 1-1',
        'menu1-2': 'Menu 1-2',
        'menu1-2-1': 'Menu 1-2-1',
        'menu1-2-2': 'Menu 1-2-2',
        'menu1-3': 'Menu 1-3',
        menu2: 'Menu 2',
        tab: 'Tab',
        form: 'Form',
        page401: '401',
        page404: '404',
        externalLink: 'External Link',
        permission: 'Permission',
        rolePermission: 'Role Permission',
    },
    // 系統管理 -> 角色管理 中的新增/編輯 Dialog 中的 el-tree 所用的多語系
    roleTree: {
        dashboard: 'Dashboard',

        systemManagement: 'System Management',
        user: 'User',
        role: 'Role',
        userAccount: 'User Account',

        orderManagement: 'Order Management',
        orderTrackingForCustomerService: '[Customer Service]Order Tracking',
        orderTrackingForCustomer: '[Customer]Order Tracking',
        unreportedOrders: 'Unreported Orders Tracking',

        logisticianAndVehicleManagement: 'Logistician And Vehicle Management',
        logisticianManagement: 'Logistician Management',
        vehicleManagement: 'Vehicle Management',

        exceptionManagement: 'Exception Management',
        goodsPackageReceiptSearch: 'Goods Package Receipt Search',
        intervalAbnormalContactForm: 'Interval Abnormal Contact Form',

        reportManagement: 'Report Management',
        collectionSummary: 'Collection Summary',
    },
    login: {
        title: 'Login Form',
        logIn: 'Login',
        username: 'Username',
        password: 'Password',
        printLogbook: 'Print Logbook',
        any: 'any',
        thirdpartyTips: 'Can not be simulated on local, so please combine you own business simulation! ! !',
        switchLanguageSuccess: 'Switch English Language',
    },
    navbar: {
        dashboard: 'Dashboard',
        logOut: 'Log Out',
    },
    dashboard: {
        name: 'Name',
        roles: 'Roles'
    },
    tagsView: {
        close: 'Close',
        closeOthers: 'Close Others',
        closeAll: 'Close All'
    },
    message: {
        success: {
            search: 'search success',
            accountUpdate: 'User modified successfully'
        },
        fail: {
            search: 'search fail',
        },
        waring: {
            deliveryNoteNumber: 'waring！need to fill in delivery note number',
            orderNumber: 'waring！need to fill in Order Number',
            exportPdf: 'Please select the interval exception contact form to be exported',
        },
        loading: {
            dataProcess: 'Data processing...'
        }
    },
    button: {
        export: 'Export',
        confirmUpdate: 'Confirm Update',
    },
    pageContent: {
        consolidationList: 'consolidation List',
        consolidationDC: 'Consolidation DC',
        orderDate: 'Order Date',
        deliveryDate: 'Delivery Date',
        oldPassword: 'Old Password',
        repassword: 'Repeat Password',
        newPassword: 'New Password',
        loginAccount: 'Login Account',
        userName: 'User Name',
        to: 'To',
        needInput: 'Need Input',
        costcoDailyDistributionStatistics: 'costco Daily Distribution Statistics'
    }
}