import { login, webLogin, logout, getInfo, getUserPermission } from '@/api/user'
import { getToken, setToken, removeToken, getDcCode, setDcCode, removeDcCode } from '@/utils/auth'
import { resetRouter } from '@/router'
import jwt_decode from 'jwt-decode';

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: '',
    expireTime: '',
    roles: [],
    dcCode: getDcCode()
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_NICKNAME: (state, nickname) => {
    state.nickname = nickname
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_EXPIRE_TIME: (state, expireTime) => {
    state.expireTime = expireTime
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_PERMISSION: (state, permissions) => {
    state.permissions = permissions
  },
  SET_DC_CODE: (state, dcCode) => {
    state.dcCode = dcCode
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password, dcCode } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(response => {
        const { data } = response
        commit('SET_TOKEN', data)
        commit('SET_DC_CODE', dcCode)
        setToken(data)
        setDcCode(dcCode)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  webLogin({ commit }, data) {
    return new Promise((resolve, reject) => {
      webLogin(data).then(response => {
        if (response.data !== null) {
          commit('SET_NICKNAME', response.data)
          resolve()
        } else {
          reject(response.msg)
        }
      }).catch(error => {
        reject(error)
      })
    })
  },

  getUserPermission({ commit }) {
    return new Promise((resolve, reject) => {
      getUserPermission(state.roles).then(response => {
        commit('SET_PERMISSION', response.data)
        resolve(response.data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      const data = jwt_decode(state.token);
      if (!data) {
        reject('Verification failed, please Login again.')
      }

      const { roleIds, roles, username, avatar, exp, iat } = data;

      // // 測試過期 token
      // var loginTime = new Date(iat * 1000);
      // console.log(iat); // token 登入時間
      // console.log(loginTime); // token 登入時間
      // loginTime.setSeconds(loginTime.getSeconds() + 2); // 過期時間: 登入時間 + 5 秒
      // const expTime = Math.floor(new Date(loginTime) / 1000);
      // console.log(expTime); // token 過期時間
      // console.log(new Date(expTime * 1000));
      // commit('SET_EXPIRE_TIME', expTime)

      commit('SET_EXPIRE_TIME', exp)
      //commit('SET_ROLES', ["admin"])
      commit('SET_ROLES', roleIds)
      commit('SET_NAME', username)
      commit('SET_AVATAR', "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
      resolve(data);

      // getInfo(state.token).then(response => {
      //   const { data } = response

      //   if (!data) {
      //     reject('Verification failed, please Login again.')
      //   }

      //   const { roles, name, avatar } = data

      //   // roles must be a non-empty array
      //   if (!roles || roles.length <= 0) {
      //     reject('getInfo: roles must be a non-null array!')
      //   }

      //   commit('SET_ROLES', roles)
      //   commit('SET_NAME', name)
      //   commit('SET_AVATAR', avatar)
      //   resolve(data)
      // }).catch(error => {
      //   reject(error)
      // })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      removeToken() // must remove  token  first
      removeDcCode()
      resetRouter()
      commit('RESET_STATE')
      resolve()
      // TODO 尚未完成「登出」功能
      // logout(state.token).then(() => {
      //   removeToken() // must remove  token  first
      //   resetRouter()
      //   commit('RESET_STATE')
      //   resolve()
      // }).catch(error => {
      //   reject(error)
      // })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

