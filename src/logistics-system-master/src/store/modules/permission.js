import { asyncRoutes, constantRoutes } from '@/router'
/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}

/**
 * 將取得使用者的權限填入 roles
 * @param routes asyncRoutes
 * @param permissions 權限
 */
export function setPermissionRoutes(routes, permissions) {
  let per = permissions;
  let rootKeyList = ['2000', '3000'];
  routes.forEach(route => {
    if (route.key) {
      permissions.forEach(per => {
        if (_.includes(rootKeyList, route.key)) {
          route.childrenKey.forEach(ck => {
            if (_.includes(per.resource, ck) && !_.includes(route.meta.roles, per.roleId)) {
              route.meta.roles.push(per.roleId)
            }
          })
        }
        else if (_.includes(per.resource, route.key)) {
          route.meta.roles.push(per.roleId)
        }
      })
      if (route.children) setPermissionRoutes(route.children, per);
    }
  })
  return asyncRoutes
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes jsonRoutes
 * @param roles
 */
export function filterJsonRoutes(routes, roles) {
  const res = []
  routes.forEach(route => {
    const tmp = { ...route }
    if (hasPermission(roles, tmp)) {
      if (tmp.children) {
        tmp.children = filterJsonRoutes(tmp.children, roles)
      }
      res.push(tmp)
    }
  })
  return res
}

const state = {
  routes: [],
  addRoutes: []
}

// 當前登入者權限
const actions = {
  generateRoutes({ commit }, data) {
    return new Promise(resolve => {
      let accessedRoutes = filterJsonRoutes(setPermissionRoutes(asyncRoutes, data.per), data.roles);
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  }
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes // 功能節點結構
    state.routes = constantRoutes.concat(routes) // 全部頁面節點，包含登入、404...
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
