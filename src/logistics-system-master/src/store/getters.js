const getters = {
  sidebar: state => state.app.sidebar,
  language: state => state.app.language,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  nickname: state => state.user.nickname,
  roles: state => state.user.roles,
  expireTime: state => state.user.expireTime,
  permissions: state => state.user.permissions,
  dcCode: state => state.user.dcCode,
  permission_routes: state => state.permission.routes
}
export default getters
