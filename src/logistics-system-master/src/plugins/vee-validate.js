//How to import rules: https://logaretm.github.io/vee-validate/guide/rules.html#importing-the-rules
import * as rules from 'vee-validate/dist/rules';
import { extend } from "vee-validate";
import { localize } from 'vee-validate';
import zh_TW from 'vee-validate/dist/locale/zh_TW';

//Sets the localization
localize({
  zh_TW
});
localize("zh_TW");

//Import all available rules
Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});
